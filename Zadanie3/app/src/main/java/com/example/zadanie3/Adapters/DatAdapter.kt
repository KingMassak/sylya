package com.example.zadanie3.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.zadanie3.R
import com.example.zadanie3.SetInitialData

class DatAdapter(context: Context?, var items: List<SetInitialData>) : RecyclerView.Adapter<DatAdapter.ViewHolder>() {
    private val inflater: LayoutInflater
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.fragment_1, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val items1 = items[position]
        holder.temp.text = items1.temp
        holder.temp1.text = items1.temp_min
        holder.image.setImageResource(items1.image)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        val temp: TextView
        val temp1: TextView
        val image: ImageView

        init {
            temp = view.findViewById<View>(R.id.textView8) as TextView
            temp1 = view.findViewById<View>(R.id.textView9) as TextView
            image = view.findViewById(R.id.imageView5)
        }
    }

    init {
        inflater = LayoutInflater.from(context)
    }
}