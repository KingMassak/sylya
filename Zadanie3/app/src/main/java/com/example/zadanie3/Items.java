package com.example.zadanie3;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.List;

public class Items {
    public static class WeatherTemp {
        Double temp;
        Double feels_like;
    }
    public static class WeatherIcon{
        String icon;
        String description;
    }
    @SerializedName("dt")
    private long timestamp;

    @SerializedName("main")
    private WeatherTemp temp;

    @SerializedName("weather")
    private List<WeatherIcon> description1;


    public Items(WeatherTemp temp, List<WeatherIcon> description) {
        this.temp = temp;
        this.description1 = description;
    }
    public String getTempWithDegree() { return String.valueOf(temp.temp.intValue()) + "\u00B0"; }
    public String getTemmax(){return String.valueOf ( temp.feels_like )+"\u00B0";}

    public String getDescription() { return description1.get(0).description; }

    public String getIconUrl() {
        return "ic_" + description1.get(0).icon;
    }

    public Calendar getDate() {
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timestamp * 1000);
        return date;
    }
}
