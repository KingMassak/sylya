package com.example.zadanie3.Retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SingRetrofit {
    private static SingRetrofit mInstance;
    private Gson gson;
    private static final String BASE_URL="https://api.openweathermap.org/data/2.5/";
    private  Retrofit retrofit;


    private SingRetrofit()
    {

        gson = new GsonBuilder ().setLenient ().create ();
        retrofit = new Retrofit.Builder ().baseUrl ( BASE_URL ).addConverterFactory ( GsonConverterFactory.create ( gson ) ).addCallAdapterFactory ( RxJava3CallAdapterFactory.create () ).build ();

    }
    public static SingRetrofit getInstance()
    {

        if(mInstance == null) mInstance = new SingRetrofit ();
        return mInstance;
    }
      public WheterApi wheterApi(){
        return retrofit.create ( WheterApi.class );
    }

}
