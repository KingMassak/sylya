package com.example.zadanie3.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ViewFlipper
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.zadanie3.Adapters.DataAdapter
import com.example.zadanie3.R
import com.example.zadanie3.R.drawable
import com.example.zadanie3.Retrofit.SingRetrofit
import com.example.zadanie3.Retrofit.WheterApi
import com.example.zadanie3.Retrofit.getUrl
import com.example.zadanie3.SetInitialData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.observers.DisposableObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_2.*
import java.util.*

class PageFragment : Fragment() {
    private val wheterApi: WheterApi
    private val singRetrofit: SingRetrofit
    var items = ArrayList<SetInitialData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_2, null)
        val cm = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (wifiInfo != null && wifiInfo.isConnected) {
            items.clear()
            val adapter = DataAdapter(this.context, items)
            wheterApi.loadGomel().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : DisposableObserver<getUrl?>() {
                override fun onNext(getUrl: @NonNull getUrl?) {
                    var dat = ""
                    val date = Calendar.getInstance()
                    for (weather in getUrl!!.items) {
                        var image = 0
                        try {
                            image = drawable::class.java.getField(weather.iconUrl).getInt(null)
                        } catch (e: IllegalAccessException) {
                            e.printStackTrace()
                        } catch (e: NoSuchFieldException) {
                            e.printStackTrace()
                        }
                        if (weather.date[Calendar.HOUR_OF_DAY] == 15) {
                            val data = weather.date[Calendar.DAY_OF_MONTH].toString() + "." + weather.date[Calendar.MONTH] + "." + weather.date[Calendar.YEAR]
                            when (weather.date[Calendar.DAY_OF_WEEK]) {
                                2 -> {
                                    dat = "Понедельник \n$data"
                                }
                                3 -> {
                                    dat = "Вторник \n$data"
                                }
                                4 -> {
                                    dat = "Среда \n$data"
                                }
                                5 -> {
                                    dat = "Четверг \n$data"
                                }
                                6 -> {
                                    dat = "Пятница \n$data"
                                }
                                7 -> {
                                    dat = "Суббота \n$data"
                                }
                                1 -> {
                                    dat = "Воскресенье \n$data"
                                }
                            }
                            items.add(SetInitialData(weather.tempWithDegree, dat, image))
                        }
                    }
                    items.removeAt(4)
                    items.removeAt(3)
                    recyclerView2.setAdapter(adapter)
                }

                override fun onError(e: @NonNull Throwable?) {}
                override fun onComplete() {
                    viewFlipper1.showNext()
                }
            })
        } else {
            Handler().postDelayed({
                val ft = fragmentManager!!.beginTransaction()
                ft.detach(this@PageFragment)
                ft.attach(this@PageFragment)
                ft.commit()
            }, 1000)
        }
        return view
    }

    init {
        singRetrofit = SingRetrofit.getInstance()
        wheterApi = singRetrofit.wheterApi()
    }
}