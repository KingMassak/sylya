package com.example.zadanie3.Retrofit;

import com.example.zadanie3.Items;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface WheterApi {
    @GET("forecast?q=Gomel&lang=ru&appid=a7ba442f071071e6d0a3ba2abb163823&units=metric")
    Observable<getUrl> loadGomel();

    @GET("weather?q=Gomel&lang=ru&appid=a7ba442f071071e6d0a3ba2abb163823&units=metric")
    Observable<Items> loadGo();
}