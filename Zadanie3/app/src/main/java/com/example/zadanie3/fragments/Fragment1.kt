package com.example.zadanie3.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.ViewFlipper
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.example.zadanie3.Adapters.DatAdapter
import com.example.zadanie3.Items
import com.example.zadanie3.R
import com.example.zadanie3.R.drawable
import com.example.zadanie3.Retrofit.SingRetrofit
import com.example.zadanie3.Retrofit.WheterApi
import com.example.zadanie3.Retrofit.getUrl
import com.example.zadanie3.SetInitialData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.observers.DisposableObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.razmetka.*
import kotlinx.android.synthetic.main.razmetka.view.*
import java.util.*

class Fragment1 : Fragment(), OnRefreshListener {
    private val wheterApi: WheterApi
    private val singRetrofit: SingRetrofit
    //val SwipeRefreshLayout swipeRefresh;
    var items = ArrayList<SetInitialData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.razmetka, null)

        val cm = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (wifiInfo != null && wifiInfo.isConnected) {
            items.clear()
            val adapter = DatAdapter(view.context, items)
            wheterApi.loadGomel().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : DisposableObserver<getUrl?>() {
                override fun onNext(getUrl: @NonNull getUrl?) {
                    var dat = ""
                    val date = Calendar.getInstance()
                    val dataaa = date[Calendar.DAY_OF_MONTH]
                    for (weather in getUrl!!.items) {
                        var image = 0
                        try {
                            image = drawable::class.java.getField(weather.iconUrl).getInt(null)
                        } catch (e: IllegalAccessException) {
                            e.printStackTrace()
                        } catch (e: NoSuchFieldException) {
                            e.printStackTrace()
                        }
                        if (weather.date[Calendar.DAY_OF_MONTH] == dataaa) {
                            dat = weather.date[Calendar.HOUR_OF_DAY].toString() + ":00"
                            items.add(SetInitialData(weather.tempWithDegree, dat, image))
                        }
                        recyclerView1.adapter = adapter
                        imageView.setImageResource(image)
                    }
                }

                override fun onError(e: @NonNull Throwable?) {}
                override fun onComplete() {}
            })
            wheterApi.loadGo().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : DisposableObserver<Items?>() {
                override fun onNext(items: @NonNull Items?) {

                    temp.setText(items!!.tempWithDegree)
                    pogoda.setText(items.description.toUpperCase())
                }

                override fun onError(e: @NonNull Throwable?) {}
                override fun onComplete() {
                    viewFlipper2.showNext()
                }
            })
        } else {
            onRefresh()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeRefresh!!.setOnRefreshListener{
           onRefresh();
        }
    }



    init {
        singRetrofit = SingRetrofit.getInstance()
        wheterApi = singRetrofit.wheterApi()
    }

    override fun onRefresh() {
        Handler().postDelayed({
            swipeRefresh!!.isRefreshing = false
            val ft = fragmentManager!!.beginTransaction()
            ft.detach(this@Fragment1)
            ft.attach(this@Fragment1)
            ft.commit()
        }, 1000)
}
}


