package com.example.zadanie3.Retrofit;

import com.example.zadanie3.Items;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class getUrl {
        @SerializedName("list")
        private List<Items> items;

        public getUrl(List<Items> items) {
            this.items = items;
        }

        public List<Items> getItems() {
            return items;
        }
    }