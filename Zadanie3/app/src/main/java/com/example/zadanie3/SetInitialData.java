package com.example.zadanie3;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SetInitialData  {
    private String temp, temp_min;
    private int image;


    @Override
    public String toString () {
        return "SetInitialData{" +
                "temp='" + temp + '\'' +
                ", temp_min='" + temp_min + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public SetInitialData ( String temp, String temp_min, int image) {

        this.temp = temp;
        this.temp_min = temp_min;
        this.image=image;
    }

    public String getTemp () {
        return this.temp;
    }

    public void setTemp ( String temp ) {
        this.temp = temp;
    }

    public String getTemp_min () {
        return this.temp_min;
    }

    public void setTemp_min ( String temp_min ) {
        this.temp_min = temp_min;
    }

    public int getImage () {
        return this.image;
    }

    public void setImage ( int image ) {
        this.image = image;
    }

}
