package com.example.zadanie3

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.zadanie3.Adapters.MyAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val adapter = MyAdapter(supportFragmentManager)
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = adapter
        viewPager.currentItem = 0
        tabs.setupWithViewPager(viewPager)
    }
}