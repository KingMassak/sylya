package com.example.zadanie3.Adapters

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.zadanie3.fragments.Fragment1
import com.example.zadanie3.fragments.Fragment2
import com.example.zadanie3.fragments.PageFragment

class MyAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getCount(): Int {
        return 3
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> Fragment1()
            1 -> PageFragment()
            2 -> Fragment2()
            else ->throw IllegalStateException("position $position is invalid for this viewpager");
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Текущая"
            1 -> "3 дня"
            2 -> "5 дней"
            else -> null
        }
    }
}